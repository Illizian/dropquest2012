var number = 0;
var results = [];
var t1 = 0, t2 = 0, t3 = 0, t4 = 0, t5 = 0;

while(check(number) !== true) {
	if(number === 99999) {
		console.log(results)
		console.log('rule counters- r1: ' + t1 + ' r2: ' + t2 + ' r3: ' + t3 + ' r4: ' + t4  + ' r5: ' + t5)
		process.exit(1)
	}
	number++;
	//console.log(number);
}

function check(number) {
	var r1, r2, r3, r4, r5;
	var numberstr = number.toString()
	var n1 = Number(numberstr.substr(0, 1)), n2 = Number(numberstr.substr(1, 1)), n3 = Number(numberstr.substr(2, 1)), n4 = Number(numberstr.substr(3, 1)), n5 = Number(numberstr.substr(4, 1));
		
	//Rule 1 - The product of the first two digits is 24.
		if(n1 * n2 === 24) {
			//console.log("Rule 1 Matched")
			r1 = true;
			t1++;
		}
    //Rule 2 - The fourth digit is half of the second digit.
    	if(n4 == (n2 / 2)) {
			//console.log("Rule 2 Matched!")
			r2 = true;
			t2++;
		}
    //Rule 3 - The sum of the last two digits is the same as the sum of the first and third digits.
    	if((n4 + n5) === (n1 + n3)) {
    		//console.log("Rule 3 Matched!")
			r3 = true;
			t3++;
    	}
    //Rule 4 - The sum of all the digits is 26.
    	if((n1+n2+n3+n4+n5) == 26) {
    		console.log("Rule 4 Matched! " + number)
			r4 = true;
			t4++;
    	}
    //Rule 5 - Not all the digits are unique.
    	//r5 = true
    //Test our results
    //result(number, r1, r2, r3, r4, r5)
    if(r1 === true && r2 === true && r3 === true && r4 === true /*&& r5 === true*/) {
    	results.push( [number, r1, r2, r3, r4, r5] )
    	console.log('Stored result!')
    }
    //if(r1 === true && r2 === true && r3 === true && r4 === true) { results.push(number); }
}

function result(no, r1, r2, r3, r4, r5) {
    //console.log('1: ' + r1 + '2: ' + r2 + '3: ' + r3 + '4: ' + r4 + '5: ' + r5)
}